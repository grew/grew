# test the usage of % symbol in lexicons

The symbol `%` can be used in a lexicon, but it should be escaped (`\%`) if it is in the first column.