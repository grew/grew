# Test the behaviour of two successive commands edge_feat_update on the same edge

* `01`: check that the final edge is correct
* `02`: the final edge is identical to the produced edge: only one edge is produced
* `03`: an intermediate edge is identical to the produced edge: only one edge is produced

Note: it the two commands are swapped, the output is different in test `03`.
